from tkinter import *

import requests
from requests.auth import HTTPBasicAuth
import urllib3
import json
import time

urllib3.disable_warnings()


def get_dnac_token():
    response = requests.post(BASE_URL + AUTH_URL,auth=HTTPBasicAuth(USERNAME, PASSWORD),verify=False)
    token = response.json()['Token']

    return token


def entries_data_get():
    global data_get_all_entries
    data_get_all_entries=[AREA0_entry.get(),AREA1_entry.get(),AREA2_entry.get(),BLD_NAME_entry.get(),BLD_LAT_entry.get(), BLD_LONG_entry.get()]
    print (data_get_all_entries)

def execute_all():
    print ("Executing")
    root.quit()

#DNAC_DATA

root=Tk()
root.title("EMEA Cluster SDA Automation")
root.geometry("1200x900")
root.resizable(False,False)

###################NETWORK DESIGN#########################
#HIERARCHY - LABELS AND GRID TKINTER


TITLE_DESIGN_label=Label(root, text="NETWORK HIERARCHY", font="bold", padx=60)
TITLE_DESIGN_label.grid(row=0,column=0, columnspan=2)
AREA0_label=Label(root, text="EMEA/")
AREA0_label.grid(row=1,column=0)
AREA0_entry=Entry(root, width=30)
AREA0_entry.grid(row=1,column=1)

AREA1_label=Label(root, text="subarea:")
AREA1_label.grid(row=2,column=0)
AREA1_entry=Entry(root, width=30)
AREA1_entry.grid(row=2,column=1)

AREA2_label=Label(root, text="subarea:")
AREA2_label.grid(row=3,column=0)
AREA2_entry=Entry(root, width=30)
AREA2_entry.grid(row=3,column=1, columnspan=2)

BLD_NAME_label=Label(root, text="Building:")
BLD_NAME_label.grid(row=4,column=0)
BLD_NAME_entry=Entry(root, width=30)
BLD_NAME_entry.grid(row=4,column=1, columnspan=2)

BLD_LAT_label=Label(root, text="Build LAT")
BLD_LAT_label.grid(row=5,column=0)
BLD_LAT_entry=Entry(root, width=15)
BLD_LAT_entry.grid(row=6,column=0)
BLD_LONG_label=Label(root, text="Build LONG")
BLD_LONG_label.grid(row=5,column=1)
BLD_LONG_entry=Entry(root, width=15)
BLD_LONG_entry.grid(row=6,column=1)


button_store_data = Button(root, text="Submit", width=10, command=entries_data_get)
button_store_data.grid(row=7, column=0)

execute=Button(root, text="quit", width=10, command=execute_all)
execute.grid(row=7, column=1)

root.mainloop()

#HIERARCHY - DNAC API


BASE_URL = 'https://url'
AUTH_URL = '/dna/system/api/v1/auth/token'
USERNAME = 'xx@corp'
PASSWORD = 'xxxx'
headers = {'X-Auth-Token': get_dnac_token(), 'Content-Type': 'application/json'}

site_url = '/dna/intent/api/v1/site'

site_headers = headers
site_headers['__runsync'] = 'true'
site_headers['__runsynctimeout'] = '30'



area0= {
    "type": "area",
    "site": {
        "area": {
            "name": data_get_all_entries[0],
            "parentName": "Global/EMEA"
        }
    }
}

area1 = {
    "type": "area",
    "site": {
        "area": {
            "name": data_get_all_entries[1],
            "parentName": "Global/EMEA/" + str(data_get_all_entries[0])
        }
    }
}

area2 = {
    "type": "area",
    "site": {
        "area": {
            "name": data_get_all_entries[2],
            "parentName": "Global/EMEA/" + str(data_get_all_entries[0]) + "/" + str(data_get_all_entries[1])
        }
    }
}


if data_get_all_entries[1]=="" and data_get_all_entries[2] == "":
    print ("TEST!!!!!!!!!!!!!!!!!!!")
    site_building = {
        "type": "building",
        "site": {
            "building": {
                "name": data_get_all_entries[3],
                "parentName": "Global/EMEA/" + data_get_all_entries[0],
                "latitude": data_get_all_entries[4],
                "longitude": data_get_all_entries[5]
            }
        }
    }


elif data_get_all_entries[1]!="" and data_get_all_entries[2] == "":
    site_building = {
        "type": "building",
        "site": {
            "building": {
                "name": data_get_all_entries[3],
                "parentName": "Global/EMEA/" + data_get_all_entries[0] + "/" + data_get_all_entries[1],
                "latitude": data_get_all_entries[4],
                "longitude": data_get_all_entries[5]
            }
        }
    }


elif data_get_all_entries[1] != "" and data_get_all_entries[2] != "":
    site_building = {
        "type": "building",
        "site": {
            "building": {
                "name": data_get_all_entries[3],
                "parentName": "Global/EMEA/" + data_get_all_entries[0] + "/" + data_get_all_entries[1]+ "/" + data_get_all_entries[2],
                "latitude": data_get_all_entries[4],
                "longitude": data_get_all_entries[5]
            }
        }
    }



if data_get_all_entries[0] !="" and data_get_all_entries[1] =="" and data_get_all_entries[2] == "":
    print ("var1")
    response = requests.post(BASE_URL + site_url,headers=site_headers, json=area0, verify=False)
    time.sleep(4)

    print (response.json())
    print(response.text.encode('utf8'))
    response = requests.post(BASE_URL + site_url,headers=site_headers, json=site_building,verify=False)
    time.sleep(4)
    print(response.json())
    print(response.text.encode('utf8'))
    time.sleep(4)
    print ("Area0" +str(area0))
    print("BLD " + str(site_building))

elif data_get_all_entries[0] !="" and data_get_all_entries[1] !="" and data_get_all_entries[2] == "":
    print("var2")
    response = requests.post(BASE_URL + site_url, headers=site_headers, json=area0, verify=False)
    response = requests.post(BASE_URL + site_url, headers=site_headers, json=area1, verify=False)
    response = requests.post(BASE_URL + site_url, headers=site_headers, json=site_building, verify=False)

elif data_get_all_entries[0] !="" and data_get_all_entries[1] !="" and data_get_all_entries[2] != "":
    print("var3")
    response = requests.post(BASE_URL + site_url, headers=site_headers, json=area0, verify=False)
    response = requests.post(BASE_URL + site_url, headers=site_headers, json=area1, verify=False)
    response = requests.post(BASE_URL + site_url, headers=site_headers, json=area2, verify=False)
    response = requests.post(BASE_URL + site_url, headers=site_headers, json=site_building, verify=False)


